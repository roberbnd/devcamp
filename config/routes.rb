Rails.application.routes.draw do
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }
  resources :portafolios, except: [:show]

  get 'about', to: 'pages#about'
  get 'contact', to: 'pages#contact'

  get 'react-items', to: 'portafolios#react'
  get 'portafolio/:id', to: 'portafolios#show', as: 'portafolio_show'

  resources :blogs do
    member do
      get :toggle_status
    end
  end

  root to: 'pages#home'
end
