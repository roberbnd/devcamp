module ApplicationHelper
  def login_helper
    if current_user.is_a?(User)
      link_to 'Logout', destroy_user_session_path, method: :delete
    else
      (link_to 'Login', new_user_session_path) +
        "<br>".html_safe +
      (link_to 'Register', new_user_registration_path)
    end
  end

  def source_helper(layout)
    if session[:source]
      content_tag(:p, "Your session is #{session[:source]} on the layout #{layout}")
    end
  end

  def copyright_generator_helper
    DevcampViewHtmlgenerator::Renderer.copyright('Myname', 'MyMsg')
  end
end
