class PortafoliosController < ApplicationController
  layout 'portafolio'

  def index
    @portafolio_items = Portafolio.all
  end

  def react
    @portafolio_items = Portafolio.react
  end

  def new
    @portafolio_item = Portafolio.new
    3.times { @portafolio_item.technologies.build }
  end

  def create
    @portafolio_item = Portafolio.new(portofolio_params)

    respond_to do |format|
      if @portafolio_item.save
        format.html { redirect_to portafolios_path, notice: 'Portafolio item was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def edit
    @portafolio_item = Portafolio.find(params[:id])
  end

  def update
    @portafolio_item = Portafolio.find(params[:id])
    respond_to do |format|
      if @portafolio_item.update(portofolio_params)
        format.html { redirect_to portafolios_path, notice: 'The record was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def show
    @portafolio_item = Portafolio.find(params[:id])
  end

  def destroy
    @portafolio_item = Portafolio.find(params[:id])

    @portafolio_item.destroy

    respond_to do |format|
      format.html { redirect_to portafolio_url, notice: 'Portfolio item was successfully destroyed.' }
    end
  end

  private

  def portofolio_params
    params.require(:portafolio).permit(:title,
                                       :subtitle,
                                       :body,
                                       :image,
                                       technologies_attributes: [:name])
  end
end
