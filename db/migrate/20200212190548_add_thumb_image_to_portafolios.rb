class AddThumbImageToPortafolios < ActiveRecord::Migration[6.0]
  def change
    add_column :portafolios, :thumb_image, :string
  end
end
