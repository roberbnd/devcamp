class CreatePortafolios < ActiveRecord::Migration[6.0]
  def change
    create_table :portafolios do |t|
      t.string :title
      t.string :subtitle
      t.text :body
      t.string :main_image
      t.string :thumbl_image

      t.timestamps
    end
  end
end
