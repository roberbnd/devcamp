3.times do |topic|
  Topic.create!(
    title: 'Topic #{topic}'
  )
end

puts "5 topics created"

10.times do |blog|
  Blog.create!(
    title: "My Blog Post #{blog}",
    body: "In hac habitasse platea dictumst. Nam posuere ultricies turpis.
      Pellentesque a pede. Duis sed tortor. Phasellus egestas porta lectus.
      Aliquam dignissim consequat diam. Pellentesque pede.
      Ut varius tincidunt tellus. Curabitur ornare ipsum. Aenean laoreet posuere
      orci. Etiam id nisl. Suspendisse volutpat elit molestie orci. Suspendisse
      vel augue at felis tincidunt sollicitudin. Fusce arcu. Duis a tortor. Duis
      et odio et leo sollicitudin consequat. Aliquam lobortis. Phasellus
      condimentum. Ut porttitor bibendum libero. Integer euismod lacinia velit.
        Donec velit justo, sodales varius, cursus sed, mattis a, arcu.
      Maecenas accumsan, sem iaculis egestas gravida, odio nunc aliquet dui, eget
      cursus diam purus vel augue. Donec eros.",
    topic_id: Topic.last.id
  )
end

puts " 10 blog posts"

5.times do |skill|
  Skill.create!(
    title: "Rails #{skill}",
    percent_utilized: 13
  )
end

puts "5 skills created"

1.times do |portafolio_item|
  Portafolio.create!(
    title: "Portafolio title: #{portafolio_item}",
    subtitle: "Ruby",
    body: " Facilisis ante nisi eget lectus. Sed a est. Aliquam nec felis eu
      sem euismod viverra. Suspendisse felis mi, dictum id, convallis ac, mattis non,
      nibh. Donec sagittis quam eu mauris. Phasellus et leo at quam dapibus
      pellentesque. In non lacus. Nullam tristique nunc ut arcu scelerisque
      aliquam. Nullam viverra magna vitae leo. Vestibulum in lacus sit amet
      lectus tempus aliquet. Duis cursus nisl ac orci. Donec non nisl. Mauris
      lacus sapien, congue a, facilisis at, egestas vel, quam. Vestibulum ante
      ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.
      Phasellus ipsum odio, suscipit nec, fringilla at, vehicula quis, tellus.
      Phasellus.",
      main_image:  "https://via.placeholder.com/600x400",
      thumb_image: "https://via.placeholder.com/350x200"
  )
end

puts "1 portafolio item created"

8.times do |portafolio_item|
  Portafolio.create!(
    title: "Portafolio title: #{portafolio_item}",
    subtitle: "React",
    body: " Facilisis ante nisi eget lectus. Sed a est. Aliquam nec felis eu
      sem euismod viverra. Suspendisse felis mi, dictum id, convallis ac, mattis non,
      nibh. Donec sagittis quam eu mauris. Phasellus et leo at quam dapibus
      pellentesque. In non lacus. Nullam tristique nunc ut arcu scelerisque
      aliquam. Nullam viverra magna vitae leo. Vestibulum in lacus sit amet
      lectus tempus aliquet. Duis cursus nisl ac orci. Donec non nisl. Mauris
      lacus sapien, congue a, facilisis at, egestas vel, quam. Vestibulum ante
      ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.
      Phasellus ipsum odio, suscipit nec, fringilla at, vehicula quis, tellus.
      Phasellus.",
      main_image:  "https://via.placeholder.com/600x400",
      thumb_image: "https://via.placeholder.com/350x200"
  )
end

puts "8 portafolio item created"

3.times do |technology|
  Technology.create!(
    name: "Technology #{technology}",
    portafolio_id: Portafolio.last.id
  )
end

puts "3 technologies created"
